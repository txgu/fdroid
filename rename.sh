#! /bin/bash

for APK in ./fdroid-only/*.apk
do
    echo $APK
    BASE=$(basename $APK)
    BASE=${BASE#*_}
    mv $APK ./fdroid-only/$BASE
done
