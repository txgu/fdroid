package org.javelus.crawler.fdroid;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * Hello world!
 *
 */
public class Main {

    WebDriver driver;
    
    public Main() {
        driver = createWebDriver();
    }
    
    private WebDriver createWebDriver() {
        System.setProperty("webdriver.chrome.driver", "/Users/tianxiaogu/bin/chromedriver");
        HashMap<String, Object> prefs = new HashMap<String, Object>(); 
        prefs.put("profile.managed_default_content_settings.images", 2); 
        ChromeOptions options =new ChromeOptions(); 
        options.setExperimentalOption("prefs", prefs); 
        return new ChromeDriver(options);
    }

    static By NEXT_PAGE = By.xpath("//ul[@class='browse-navigation']/li[@class='nav next']");
    static By PACKAGE_URL = By.xpath("//a[@class='package-header']");
    
    public void collectPackageURL(List<String> urls) {
        List<WebElement> webElements = driver.findElements(PACKAGE_URL);
        for (WebElement e : webElements) {
            urls.add(e.getAttribute("href"));
        }
    }
    
    public List<String> getAllURLS() throws IOException {
        driver.get("https://f-droid.org/en/packages/index.html");
        
        File urlFile = new File("package.urls.txt");
        if (urlFile.exists()) {
            return readURLSFromFile(urlFile);
        }
        
        List<String> urls = new ArrayList<String>();
        while (true) {
            WebElement next = null;
            
            try {
                next = driver.findElement(NEXT_PAGE);
            } catch (RuntimeException e) {
                
            }
            
            List<WebElement> webElements = driver.findElements(PACKAGE_URL);
            for (WebElement e : webElements) {
                urls.add(e.getAttribute("href"));
            }
            
            if (next == null) {
                break;
            }
            
            next.click();
        }
        saveURLSFromFile(urlFile, urls);
        return urls;
    }
    
    private void saveURLSFromFile(File urlFile, List<String> urls) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(urlFile))) {
            for (String url : urls) {
                bw.write(url);
                bw.newLine();
            }
        }
    }

    private List<String> readURLSFromFile(File urlFile) throws IOException {
        List<String> urls = new ArrayList<String>();
        try (BufferedReader br = new BufferedReader(new FileReader(urlFile))) {
            String line = null;
            while ((line = br.readLine())!=null) {
                urls.add(line);
            }
        }
        return urls;
    }

    public void start() throws IOException {
        List<String> urls = getAllURLS();
        for (String url : urls) {
            downloadAPK(url);
        }
    }
    
    private WebElement findElementOrNull(By by) {
        try {
            return driver.findElement(by);
        } catch (RuntimeException e) {
            return null;
        }
    }
    
    private WebElement findElementOrNull(WebElement root, By by) {
        try {
            return root.findElement(by);
        } catch (RuntimeException e) {
            return null;
        }
    }
    private static By LATEST_VERSION = By.xpath("//ul[@class='package-versions-list']/li[1]");
    private static By VERSION_HEADER = By.xpath("//div[@class='package-version-header']");
    private static By VERSION_DOWNLOAD = By.xpath("//p[@class='package-version-download']/a[1]");
    private static Pattern DATE_PATTERN = Pattern.compile("[0-9]+-[0-9]+-[0-9]+");
    private void downloadAPK(String url) throws IOException {
        System.out.println("Move to " + url);
        driver.get(url);
        WebElement latest = findElementOrNull(LATEST_VERSION);
        if (latest == null) {
            return;
        }
        WebElement header = findElementOrNull(latest, VERSION_HEADER);
        if (header == null) {
            return;
        }
        String version = header.getText();
        Matcher m = DATE_PATTERN.matcher(version);
        if (!m.find()) {
            return;
        }
        String date = m.group();
        WebElement link = findElementOrNull(latest, VERSION_DOWNLOAD);
        if (link == null) {
            return;
        }
        
        Date now = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        File output = new File("fdroid_output_" + dateFormat.format(now));
        if (!output.exists()) {
            output.mkdirs();
        }
        String name = getPackage(url);
        File download = new File(output, date + '_' + name);
        System.out.println("Get URL for " + name + ": " + link.getAttribute("href"));
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(download))) {
            bw.write(link.getAttribute("href"));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Ingore package");
        }
    }

    private String getPackage(String url) {
        int index = url.lastIndexOf('/');
        if (index == -1) {
            return "unknown.package";
        }
        return url.substring(index + 1);
    }

    public static void main( String[] args ) throws Exception {
        new Main().start();
    }
}
