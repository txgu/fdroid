package org.javelus.crawler.playstore;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * Hello world!
 *
 */
public class Main {

    WebDriver driver;
    
    public Main() {
        driver = createWebDriver();
    }
    
    private WebDriver createWebDriver() {
        System.setProperty("webdriver.chrome.driver", "/Users/tianxiaogu/bin/chromedriver");
        HashMap<String, Object> prefs = new HashMap<String, Object>(); 
        prefs.put("profile.managed_default_content_settings.images", 2); 
        ChromeOptions options =new ChromeOptions(); 
        options.setExperimentalOption("prefs", prefs); 
        return new ChromeDriver(options);
    }

    static By NEXT_PAGE = By.xpath("//ul[@class='browse-navigation']/li[@class='nav next']");
    static By PACKAGE_URL = By.xpath("//a[@class='package-header']");
    
    public void collectPackageURL(List<String> urls) {
        List<WebElement> webElements = driver.findElements(PACKAGE_URL);
        for (WebElement e : webElements) {
            urls.add(e.getAttribute("href"));
        }
    }
    
    public List<String> getAllURLS() throws IOException {
        driver.get("https://play.google.com/store/apps");
        
        File urlFile = new File("play.package.urls.txt");
        if (urlFile.exists()) {
            return readURLSFromFile(urlFile);
        }
        
        
        List<String> categories = readCategoryURLs();
        List<String> urls = new ArrayList<String>();
        for (String url : categories) {
            readURLSInCategory(url, urls);
        }
        Set<String> urlSet = new HashSet<>(urls);
        urls = new ArrayList<>(urlSet);
        Collections.sort(urls);
        saveURLSFromFile(urlFile, urls);
        return urls;
    }
    
    static By SUBCATEGORY = By.xpath("//div[@class='cluster-heading']");
    
    private void readURLSInCategory(String url, List<String> urls) {
        System.out.println("Get " + url);
        driver.get(url);
        List<String> subCategory = readSubCategoryURLs(urls);
        for (String suburl : subCategory) {
            System.out.println("Get suburl: " + suburl);
            readURLInSubCategory(suburl, urls);
        }
    }

    private void readURLInSubCategory(String url, List<String> urls) {
        driver.get(url);
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        List<WebElement> targets = driver.findElements(COVER_TARGET);
        for (WebElement e : targets) {
            urls.add(e.getAttribute("href"));
        }
    }

    static By CATEGORIES = By.xpath("//ul[@class='submenu-item-wrapper'][1]//a[@class='child-submenu-link']");
    
    private List<String> readCategoryURLs() {
        List<WebElement> elements = driver.findElements(CATEGORIES);
        List<String> urls = new ArrayList<>();
        for (WebElement e : elements) {
            String url = e.getAttribute("href");
            if (url.contains("vr_top_device_featured_category")) {
                continue;
            }
            if (url.contains("GAME")) {
                continue;
            }
            if (url.contains("ANDROID_WEAR")) {
                continue;
            }
            if (url.contains("DEMO")) {
                continue;
            }
            urls.add(url);
        }
        return urls;
    }

    static By SEE_MORE = By.xpath("//a[@class='see-more play-button small id-track-click apps id-responsive-see-more']");
    
    static By COVER_TARGET = By.xpath("//div[@class='cover']/a[@class='card-click-target']");
    
    private List<String> readSubCategoryURLs(List<String> appUrls) {
        List<WebElement> elements = driver.findElements(SEE_MORE);
        List<String> urls = new ArrayList<>();
        for (WebElement e : elements) {
            String subUrl = e.getAttribute("href");
            System.out.println("Add suburl: " + subUrl);
            urls.add(subUrl);
        }
        elements = driver.findElements(COVER_TARGET);
        for (WebElement e : elements) {
            String appUrl = e.getAttribute("href");
            System.out.println("Add appurl: " + appUrl);
            appUrls.add(appUrl);
        }
        return urls;
    }

    private void saveURLSFromFile(File urlFile, List<String> urls) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(urlFile))) {
            for (String url : urls) {
                bw.write(url);
                bw.newLine();
            }
        }
    }

    private List<String> readURLSFromFile(File urlFile) throws IOException {
        List<String> urls = new ArrayList<String>();
        try (BufferedReader br = new BufferedReader(new FileReader(urlFile))) {
            String line = null;
            while ((line = br.readLine())!=null) {
                urls.add(line);
            }
        }
        Set<String> urlSet = new HashSet<>(urls);
        urls = new ArrayList<>(urlSet);
        Collections.sort(urls);
        return urls;
    }

    public void start() throws IOException {
        List<String> urls = getAllURLS();
        for (String url : urls) {
            downloadAPK(url);
        }
    }
    
    private WebElement findElementOrNull(By by) {
        try {
            return driver.findElement(by);
        } catch (RuntimeException e) {
            return null;
        }
    }
    
    private WebElement findElementOrNull(WebElement root, By by) {
        try {
            return root.findElement(by);
        } catch (RuntimeException e) {
            return null;
        }
    }

    static By HTML_BODY = By.xpath("//body");
    private void downloadAPK(String url) throws IOException {
        Date now = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        File output = new File("play_output_" + dateFormat.format(now));
        if (!output.exists()) {
            output.mkdirs();
        }
        String name = getPackage(url);
        File download = new File(output, name);
        if (download.exists()) {
            System.out.println("Ignore content for " + name + ": " + url);
            return;
        }
        System.out.println("Move to " + url);
        driver.get(url);
        System.out.println("Get content for " + name + ": " + url);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(download))) {
            bw.write(driver.findElement(HTML_BODY).getText());
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Ingore package: " + name);
        }
    }

    private String getPackage(String url) {
        try {
            return url.split("id=")[1];
        } catch (RuntimeException e) {
            return "unknown.package";
        }
    }

    public static void main( String[] args ) throws Exception {
        new Main().start();
    }
}
