#! /bin/bash


INPUT=$1

check_code() {
    CODE=$(curl -s -o /dev/null -w "%{http_code}" $URL)
}

for PKG in $(cat $INPUT)
do
    URL=http://www.wandoujia.com/apps/$PKG

    curl $URL 2>&1 | grep "抱歉，该应用已下架" > /dev/null 2>&1
    RET=$?

    if [[ $RET == 0 ]]
    then
        WDJ=404
    else
        WDJ=200
    fi

    URL=https://f-droid.org/en/packages/$PKG
    check_code
    FDROID=$CODE

    URL=https://play.google.com/store/apps/details?id=$PKG
    check_code
    PLAY=$CODE

    printf "$PKG\t$WDJ\t$FDROID\t$PLAY\n"
done
