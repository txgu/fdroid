#! /bin/bash


LOG_DIR=./output

APK_LIST=$1

if [[ $APK_LIST == '' ]]
then
    echo "No apk list"
    exit
fi

mkdir apks

while read -r LINE
do
    URL=$(cat $LOG_DIR/$LINE)
    echo "Downloading $LINE"
    OUTPUT=./apks/$LINE.apk
    if [[ -f $OUTPUT ]]
    then
        echo "$LINE already exists."
        continue
    fi
    wget -O $OUTPUT $URL 
done < $APK_LIST
