#! /bin/bash

APK_DIR=$1

for APK in $(find $APK_DIR -name "*.apk")
do
    #aapt d xmltree $APK AndroidManifest.xml > ${APK}.log 2>&1
    #if [[ $? != 0 ]]
    #then
    #    echo "Bad app: $APK"
    #    rm -f $APK
    #    continue
    #fi
    #grep BIND_INPUT_METHOD ${APK}.log
    #if [[ $? == 0 ]]
    #then
    #    echo "InputMethod: $APK"
    #    rm -f $APK
    #    continue
    #fi

    echo $APK | grep keyboard
    if [[ $? == 0 ]]
    then
        echo "Keyboard: $APK"
        rm -f $APK
        continue
    fi
done
