#! /bin/bash

APKS_DIR=./apks

FDROID_PLAY_APKS=./fdroid-in-play

mkdir $FDROID_PLAY_APKS

check_code() {
    CODE=$(curl -s -o /dev/null -w "%{http_code}" $URL)
}

for APK in $(find $APKS_DIR -name "*-*-*_*.apk")
do
    BASENAME=$(basename $APK)
    BASENAME=${BASENAME:11}
    PKG=${BASENAME%.apk}

    URL=https://play.google.com/store/apps/details?id=$PKG
    check_code
    PLAY=$CODE

    if [[ $PLAY == 200 ]]
    then
        echo $PKG
        mv $APK ${FDROID_PLAY_APKS}/${PKG}.apk
    fi
done
