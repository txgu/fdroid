#! /bin/bash


DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)

PKG_LIST=$1

if [[ $PKG_LIST == '' ]]
then
    echo "No package list"
    exit
fi

mkdir ${DIR}/apks

for PKG in $(cat $PKG_LIST)
do

    URL=http://www.wandoujia.com/apps/$PKG

    curl $URL 2>&1 | grep "$URL/download" > /dev/null 2>&1
    RET=$?

    if [[ $RET != 0 ]]
    then
        continue
    fi

    URL=$URL/download


    if [[ -f ${DIR}/apks/$PKG.apk ]]
    then
        continue
    fi

    echo "Downloading $URL"
    wget -O ${DIR}/apks/$PKG.apk $URL 


    sleep 2s # let wandoujia happy
done
